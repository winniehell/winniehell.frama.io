#!/usr/bin/env bash

set -o errexit
set -o nounset

pandoc \
  --standalone \
  --css style.css \
  --output index.html \
  index.md
