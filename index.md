---
lang: en
title: <a href="avatar.jpg"><img aria-hidden="true" src="icon.jpg"></a>winniehell
header-includes:
- <link href="icon.jpg" rel="icon" />
- <script>
    if (location.hostname === 'winniehell.gitlab.io') {
      location.href = 'https://winniehell.de/no-redirect.html'
    }
  </script>
---

Hi, I'm Winnie!

- I like to do creative stuff
  <small>(mostly
    <a href="https://ldjam.com/users/winniehell/games">games</a>,
    <a href="https://7.5bits.winniehell.de/">music</a>,
    and <a href="#writing">writing</a>
  )</small>
- I used to work as software developer, now I'm research assistent at [Thünen-Institute](https://www.thuenen.de).
- You can contact me by e-mail: contact [Klammeraffe](https://en.wikipedia.org/wiki/At_sign) winniehell [Punkt](https://en.wikipedia.org/wiki/Full_stop) de
- Otherwise you can find me as **winniehell** on various platforms.

## Talks

- Workflows and Project Management in GitLab (English) —
  [slides](https://winnie-slides.gitlab.io/2020/gitlab-workflows-project-management-en/),
  [event](https://www.eventbrite.co.uk/e/masterclass-workflows-and-project-management-in-gitlab-tickets-86791308139)
- Game Development with Vue.js / Nuxt.js and Phaser (English) —
  [slides](https://winnie-slides.gitlab.io/2019/nuxt-phaser-games-en/),
  [recording](https://www.youtube.com/watch?v=PNhYogdvHec),
  [event](https://www.codetalks.de/program#talk-661?event=5)
- Wie macht GitLab mit GitLab GitLab? (German) —
  [slides](https://winnie-slides.gitlab.io/2019/wie-macht-gitlab-gitlab-de/),
  [event](https://www.meetup.com/de-DE/GitLab-Meetup-Hamburg/events/259759322/)
- GitLab CI Überblick (German) —
  [slides](https://winnie-slides.gitlab.io/2019/gitlab-ci-de/),
  [recording](https://www.youtube.com/watch?v=ZIPvu_KobEM),
  [event](https://stratum0.org/wiki/Vortr%C3%A4ge/Vorbei#Talks_am_Sonntag.2C_14._April_2019.2C_ab_19:00)
- Vue.js basics (German) —
  [recording](https://www.youtube.com/watch?v=rs2QbR3B2U4),
  [event](https://stratum0.org/wiki/Vortr%C3%A4ge/Vorbei#Talks_am_Sonntag.2C_14._April_2019.2C_ab_19:00)
- Wie entsteht ein GitLab feature? (German) —
  [slides](https://winnie-slides.gitlab.io/2019/gitlab-workflow-de/),
  [recording](https://www.youtube.com/watch?v=fJpR2c39qDU),
  [event](https://stratum0.org/wiki/Vortr%C3%A4ge/Vorbei#Talks_am_Donnerstag.2C_14._M.C3.A4rz_2019.2C_ab_19:00)

## Writing

- [Untitled interactive story](https://octodon.social/@winniehell/101919606721321216) (English)
- [Herr Oink von Ploink](https://winniehell.gitlab.io/oink-von-ploink/) (German, National Novel Writing Month 2018)
- [Projekt .txt](https://winniehell.gitlab.io/projekttxt/) (German, see also [project page](https://projekttxt.net/))
- [Water Child](https://winniehell.de/water-child/) (English, discontinued interactive story)
- [Sand Child](https://twitter.com/blinry/status/663798482570977280) (English, discontinued interactive story started by [\@blinry](https://morr.cc/), continued by me)

