#!/usr/bin/env bash

set -o errexit
set -o nounset

pandoc \
  --standalone \
  --css style.css \
  --output 2022-pino-beinebaumler.html \
  2022-pino-beinebaumler.md
